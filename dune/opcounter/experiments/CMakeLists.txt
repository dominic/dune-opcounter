add_executable(
  exp_experiment
  EXCLUDE_FROM_ALL
  exp_experiment.cc
  )
  
add_executable(
  trig_experiment
  EXCLUDE_FROM_ALL
  trig_experiment.cc
  )
  
if(NOT TARGET build_experiments)
  add_custom_target(build_experiments)
  set_target_properties(build_experiments PROPERTIES EXCLUDE_FROM_ALL 1)
endif()

add_dependencies(build_experiments exp_experiment)
add_dependencies(build_experiments trig_experiment)
