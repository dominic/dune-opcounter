#ifndef DUNE_OPCOUNTER_OPCOUNTER_HH
#define DUNE_OPCOUNTER_OPCOUNTER_HH

#include<dune/opcounter/scalar/definition.hh>
#include<dune/opcounter/scalar/traits.hh>
#include<dune/opcounter/scalar/operators.hh>
#include<dune/opcounter/scalar/functions.hh>

#endif
