#ifndef DUNE_OPCOUNTER_VECTOR_TRAITS_HH
#define DUNE_OPCOUNTER_VECTOR_TRAITS_HH


namespace OpCounter {
  namespace impl {

    /* A struct that maps (F, size) to the VCL boolean vector for that pair */
    template<typename F, int size>
    struct BooleanVector
    {};

    template<>
    struct BooleanVector<double, 2>
    {
      using type = VCL_NAMESPACE::Vec2db;
    };

    template<>
    struct BooleanVector<float, 4>
    {
      using type = VCL_NAMESPACE::Vec4fb;
    };

#if MAX_VECTOR_SIZE >= 256
    template<>
    struct BooleanVector<double, 4>
    {
      using type = VCL_NAMESPACE::Vec4db;
    };

    template<>
    struct BooleanVector<float, 8>
    {
      using type = VCL_NAMESPACE::Vec8fb;
    };
#endif

#if MAX_VECTOR_SIZE >= 512
    template<>
    struct BooleanVector<double, 8>
    {
      using type = VCL_NAMESPACE::Vec8db;
    };

    template<>
    struct BooleanVector<float, 16>
    {
      using type = VCL_NAMESPACE::Vec16fb;
    };
#endif

    /* A struct that maps (F, size) to the original VCL vector for that pair */
    template<typename F, int size>
    struct OriginalVector
    {};

    template<>
    struct OriginalVector<double, 2>
    {
      using type = VCL_NAMESPACE::Vec2d;
    };

    template<>
    struct OriginalVector<float, 4>
    {
      using type = VCL_NAMESPACE::Vec4f;
    };

#if MAX_VECTOR_SIZE >= 256
    template<>
    struct OriginalVector<double, 4>
    {
      using type = VCL_NAMESPACE::Vec4d;
    };

    template<>
    struct OriginalVector<float, 8>
    {
      using type = VCL_NAMESPACE::Vec8f;
    };
#endif

#if MAX_VECTOR_SIZE >= 512
    template<>
    struct OriginalVector<double, 8>
    {
      using type = VCL_NAMESPACE::Vec8d;
    };

    template<>
    struct OriginalVector<float, 16>
    {
      using type = VCL_NAMESPACE::Vec16f;
    };
#endif

    /* A struct that maps (F, size) to an VCL integer vector of the same bit size */
    template<typename F, int size>
    struct IntegerVector
    {};

    template<>
    struct IntegerVector<double, 2>
    {
      using type = VCL_NAMESPACE::Vec2q;
    };

    template<>
    struct IntegerVector<float, 4>
    {
      using type = VCL_NAMESPACE::Vec4i;
    };

#if MAX_VECTOR_SIZE >= 256
    template<>
    struct IntegerVector<double, 4>
    {
      using type = VCL_NAMESPACE::Vec4q;
    };

    template<>
    struct IntegerVector<float, 8>
    {
      using type = VCL_NAMESPACE::Vec8i;
    };
#endif

#if MAX_VECTOR_SIZE >= 512
    template<>
    struct IntegerVector<double, 8>
    {
      using type = VCL_NAMESPACE::Vec8q;
    };

    template<>
    struct IntegerVector<float, 16>
    {
      using type = VCL_NAMESPACE::Vec16i;
    };
#endif

    template<typename V>
    struct VectorSize
    {};

    template<>
    struct VectorSize<VCL_NAMESPACE::Vec2q>
    {
      enum { size = 2 };
    };

    template<>
    struct VectorSize<VCL_NAMESPACE::Vec4i>
    {
      enum { size = 4 };
    };

#if MAX_VECTOR_SIZE >= 256
    template<>
    struct VectorSize<VCL_NAMESPACE::Vec4q>
    {
      enum { size = 4 };
    };

    template<>
    struct VectorSize<VCL_NAMESPACE::Vec8i>
    {
       enum { size = 8 };
    };
#endif

#if MAX_VECTOR_SIZE >= 512
    template<>
    struct VectorSize<VCL_NAMESPACE::Vec8q>
    {
      enum { size = 8 };
    };

    template<>
    struct VectorSize<VCL_NAMESPACE::Vec16i>
    {
       enum { size = 16 };
    };
#endif
  } // namespace impl
} // namespace OpCounter

#ifdef DUNE_CODEGEN_COMMON_SIMD_TRAITS_HH

template<typename F, int size>
struct base_floatingpoint<OpCounter::impl::OpCounterVector<F, size>>
{
  using value = OpCounter::OpCounter<F>;
};

template<typename F, int size>
struct simd_size<OpCounter::impl::OpCounterVector<F, size>>
{
  static constexpr std::size_t value = size;
};

#endif

#endif
