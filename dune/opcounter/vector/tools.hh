#ifndef DUNE_OPCOUNTER_VECTOR_TOOLS_HH
#define DUNE_OPCOUNTER_VECTOR_TOOLS_HH

#include<dune/opcounter/vector/definition.hh>


namespace OpCounter{
  namespace impl {

    template<typename F, int size>
    typename OriginalVector<F, size>::type to_original(const OpCounterVector<F, size>& vec)
    {
      typename OriginalVector<F, size>::type r;
      r.load(&(vec._v[0]._v));
      return r;
    }

    template<typename F, int size, typename V>
    OpCounterVector<F, size> to_opcounted(const V& vec)
    {
      OpCounterVector<F, size> r;
      vec.store(&(r._v[0]._v));
      return r;
    }

  } // namespace impl
} // namespace OpCounter

#endif
