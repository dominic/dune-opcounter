#ifndef DUNE_OPCOUNTER_VECTOR_FUNCTIONS_HH
#define DUNE_OPCOUNTER_VECTOR_FUNCTIONS_HH

#include<algorithm>
#include<numeric>

#include<dune/opcounter/vector/definition.hh>
#include<dune/opcounter/vector/tools.hh>

#define DEFINE_OPCOUNT_LANEWISE_FUNCTION(FUNC)                                    \
  template<typename F, int size>                                                  \
  static inline OpCounterVector<F, size> FUNC(                                    \
      const OpCounterVector<F, size>& x)                                          \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounterVector<F, size> r;                                                   \
    std::transform(x._v, x._v + size, r._v, [](auto x){ return FUNC(x); });       \
    BARRIER;                                                                      \
    return r;                                                                     \
  }                                                                               \

#define DEFINE_OPCOUNT_COMPARATIVE_FUNCTION(FUNC)                                 \
  /* left: opcounted vector, right: opcounted vector */                           \
  template<typename F, int size>                                                  \
  static inline OpCounterVector<F, size> FUNC(                                    \
      const OpCounterVector<F, size>& a,                                          \
      const OpCounterVector<F, size>& b)                                          \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounterVector<F, size> r;                                                   \
    std::transform(a._v, a._v + size, b._v, r._v,                                 \
                   [](auto x, auto y){using std::FUNC; return FUNC(x, y);});      \
    BARRIER;                                                                      \
    return r;                                                                     \
  }                                                                               \
                                                                                  \
  /* left: opcounter, right: opcounted vector */                                  \
  template<typename F, int size>                                                  \
  static inline OpCounterVector<F, size> FUNC(                                    \
      const OpCounter<F>& a,                                                      \
      const OpCounterVector<F, size>& b)                                          \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounterVector<F, size> r;                                                   \
    std::transform(b._v, b._v + size, r._v,                                       \
                   [a](auto x){using std::FUNC; return FUNC(a, x);});             \
    BARRIER;                                                                      \
    return r;                                                                     \
  }                                                                               \
                                                                                  \
  /* left: opcounted vector, right: opcounter */                                  \
  template<typename F, int size>                                                  \
  static inline OpCounterVector<F, size> FUNC(                                    \
      const OpCounterVector<F, size>& a,                                          \
      const OpCounter<F>& b)                                                      \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounterVector<F, size> r;                                                   \
    std::transform(a._v, a._v + size, r._v,                                       \
                   [b](auto x){using std::FUNC; return FUNC(x, b);});             \
    BARRIER;                                                                      \
    return r;                                                                     \
  }   

#define DEFINE_OPCOUNT_FMA_FUNCTION(FUNC, UNARY, BINARY)                          \
  template<typename F, int size>                                                  \
  static inline OpCounterVector<F, size> FUNC(                                    \
      const OpCounterVector<F, size>& a,                                          \
      const OpCounterVector<F, size>& b,                                          \
      const OpCounterVector<F, size>& c)                                          \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounterVector<F, size> r;                                                   \
    for(std::size_t i=0; i < size; ++i)                                           \
      r._v[i] = UNARY a._v[i] * b._v[i] BINARY c._v[i];                           \
    BARRIER;                                                                      \
    return r;                                                                     \
  }                                                                               \
                                                                                  \
  template<typename F, int size>                                                  \
  static inline OpCounterVector<F, size> FUNC(                                    \
      const OpCounter<F>& a,                                                      \
      const OpCounterVector<F, size>& b,                                          \
      const OpCounterVector<F, size>& c)                                          \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounterVector<F, size> r;                                                   \
    for(std::size_t i=0; i < size; ++i)                                           \
      r._v[i] = UNARY a * b._v[i] BINARY c._v[i];                                 \
    BARRIER;                                                                      \
    return r;                                                                     \
  }                                                                               \
                                                                                  \
  template<typename F, int size>                                                  \
  static inline OpCounterVector<F, size> FUNC(                                    \
      const OpCounterVector<F, size>& a,                                          \
      const OpCounter<F>& b,                                                      \
      const OpCounterVector<F, size>& c)                                          \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounterVector<F, size> r;                                                   \
    for(std::size_t i=0; i < size; ++i)                                           \
      r._v[i] = UNARY a._v[i] * b BINARY c._v[i];                                 \
    BARRIER;                                                                      \
    return r;                                                                     \
  }                                                                               \
                                                                                  \
  template<typename F, int size>                                                  \
  static inline OpCounterVector<F, size> FUNC(                                    \
      const OpCounterVector<F, size>& a,                                          \
      const OpCounterVector<F, size>& b,                                          \
      const OpCounter<F>& c)                                                      \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounterVector<F, size> r;                                                   \
    for(std::size_t i=0; i < size; ++i)                                           \
      r._v[i] = UNARY a._v[i] * b._v[i] BINARY c;                                 \
    BARRIER;                                                                      \
    return r;                                                                     \
  }                                                                               \
                                                                                  \
  template<typename F, int size>                                                  \
  static inline OpCounterVector<F, size> FUNC(                                    \
      const OpCounterVector<F, size>& a,                                          \
      const OpCounter<F>& b,                                                      \
      const OpCounter<F>& c)                                                      \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounterVector<F, size> r;                                                   \
    for(std::size_t i=0; i < size; ++i)                                           \
      r._v[i] = UNARY a._v[i] * b BINARY c;                                       \
    BARRIER;                                                                      \
    return r;                                                                     \
  }                                                                               \
                                                                                  \
  template<typename F, int size>                                                  \
  static inline OpCounterVector<F, size> FUNC(                                    \
      const OpCounter<F>& a,                                                      \
      const OpCounterVector<F, size>& b,                                          \
      const OpCounter<F>& c)                                                      \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounterVector<F, size> r;                                                   \
    for(std::size_t i=0; i < size; ++i)                                           \
      r._v[i] = UNARY a * b._v[i] BINARY c;                                       \
    BARRIER;                                                                      \
    return r;                                                                     \
  }                                                                               \
                                                                                  \
  template<typename F, int size>                                                  \
  static inline OpCounterVector<F, size> FUNC(                                    \
      const OpCounter<F>& a,                                                      \
      const OpCounter<F>& b,                                                      \
      const OpCounterVector<F, size>& c)                                          \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounterVector<F, size> r;                                                   \
    for(std::size_t i=0; i < size; ++i)                                           \
      r._v[i] = UNARY a * b BINARY c._v[i];                                       \
    BARRIER;                                                                      \
    return r;                                                                     \
  }                                                                               \
                                                                                  \
  template<typename T, typename F, int size,                                      \
           typename std::enable_if<std::is_convertible<T, F>::value, int>::type = 0> \
  static inline OpCounterVector<F, size> FUNC(                                    \
      const T& a,                                                                 \
      const OpCounterVector<F, size>& b,                                          \
      const OpCounterVector<F, size>& c)                                          \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounterVector<F, size> r;                                                   \
    for(std::size_t i=0; i < size; ++i)                                           \
      r._v[i] = UNARY a * b._v[i] BINARY c._v[i];                                 \
    BARRIER;                                                                      \
    return r;                                                                     \
  }                                                                               \
                                                                                  \
  template<typename T, typename F, int size,                                      \
           typename std::enable_if<std::is_convertible<T, F>::value, int>::type = 0> \
  static inline OpCounterVector<F, size> FUNC(                                    \
      const OpCounterVector<F, size>& a,                                          \
      const T& b,                                                                 \
      const OpCounterVector<F, size>& c)                                          \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounterVector<F, size> r;                                                   \
    for(std::size_t i=0; i < size; ++i)                                           \
      r._v[i] = UNARY a._v[i] * b BINARY c._v[i];                                 \
    BARRIER;                                                                      \
    return r;                                                                     \
  }                                                                               \
                                                                                  \
  template<typename T, typename F, int size,                                      \
           typename std::enable_if<std::is_convertible<T, F>::value, int>::type = 0> \
  static inline OpCounterVector<F, size> FUNC(                                    \
      const OpCounterVector<F, size>& a,                                          \
      const OpCounterVector<F, size>& b,                                          \
      const T& c)                                                                 \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounterVector<F, size> r;                                                   \
    for(std::size_t i=0; i < size; ++i)                                           \
      r._v[i] = UNARY a._v[i] * b._v[i] BINARY c;                                 \
    BARRIER;                                                                      \
    return r;                                                                     \
  }                                                                               \
                                                                                  \
  template<typename T, typename F, int size,                                      \
           typename std::enable_if<std::is_convertible<T, F>::value, int>::type = 0> \
  static inline OpCounterVector<F, size> FUNC(                                    \
      const OpCounterVector<F, size>& a,                                          \
      const OpCounter<F>& b,                                                      \
      const T& c)                                                                 \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounterVector<F, size> r;                                                   \
    for(std::size_t i=0; i < size; ++i)                                           \
      r._v[i] = UNARY a._v[i] * b BINARY c;                                       \
    BARRIER;                                                                      \
    return r;                                                                     \
  }                                                                               \

#define IMPL_OPCOUNT_TEMPLATED_FORWARDING_VCL_FUNCTION(FUNC, COUNTER, SIZE, FTYPE)\
  template<int...INDICES,                                                         \
           typename std::enable_if<sizeof...(INDICES) == SIZE, int>::type = 0>    \
  static inline OpCounter::impl::OpCounterVector<FTYPE, SIZE> FUNC##SIZE(         \
      const OpCounter::impl::OpCounterVector<FTYPE, SIZE>& a,                     \
      const OpCounter::impl::OpCounterVector<FTYPE, SIZE>& b)                     \
  {                                                                               \
    BARRIER;                                                                      \
    auto a_ = OpCounter::impl::to_original(a);                                    \
    auto b_ = OpCounter::impl::to_original(b);                                    \
    auto r_ = VCL_NAMESPACE::FUNC##SIZE<INDICES...>(a_,b_);                       \
    OpCounter::OpCounter<FTYPE>::COUNTER(1);                                      \
    auto r = OpCounter::impl::to_opcounted<FTYPE, SIZE>(r_);                      \
    BARRIER;                                                                      \
    return r;                                                                     \
  }                                                                               \


#if MAX_VECTOR_SIZE >= 512
#define DEFINE_OPCOUNT_TEMPLATED_FORWARDING_VCL_FUNCTION(FUNC, COUNTER)           \
  IMPL_OPCOUNT_TEMPLATED_FORWARDING_VCL_FUNCTION(FUNC, COUNTER, 2, double)        \
  IMPL_OPCOUNT_TEMPLATED_FORWARDING_VCL_FUNCTION(FUNC, COUNTER, 4, float)         \
  IMPL_OPCOUNT_TEMPLATED_FORWARDING_VCL_FUNCTION(FUNC, COUNTER, 4, double)        \
  IMPL_OPCOUNT_TEMPLATED_FORWARDING_VCL_FUNCTION(FUNC, COUNTER, 8, float)         \
  IMPL_OPCOUNT_TEMPLATED_FORWARDING_VCL_FUNCTION(FUNC, COUNTER, 8, double)        \
  IMPL_OPCOUNT_TEMPLATED_FORWARDING_VCL_FUNCTION(FUNC, COUNTER, 16, float)
#elif MAX_VECTOR_SIZE >= 256
#define DEFINE_OPCOUNT_TEMPLATED_FORWARDING_VCL_FUNCTION(FUNC, COUNTER)           \
  IMPL_OPCOUNT_TEMPLATED_FORWARDING_VCL_FUNCTION(FUNC, COUNTER, 2, double)        \
  IMPL_OPCOUNT_TEMPLATED_FORWARDING_VCL_FUNCTION(FUNC, COUNTER, 4, float)         \
  IMPL_OPCOUNT_TEMPLATED_FORWARDING_VCL_FUNCTION(FUNC, COUNTER, 4, double)        \
  IMPL_OPCOUNT_TEMPLATED_FORWARDING_VCL_FUNCTION(FUNC, COUNTER, 8, float)
#else
#define DEFINE_OPCOUNT_TEMPLATED_FORWARDING_VCL_FUNCTION(FUNC, COUNTER)           \
  IMPL_OPCOUNT_TEMPLATED_FORWARDING_VCL_FUNCTION(FUNC, COUNTER, 2, double)        \
  IMPL_OPCOUNT_TEMPLATED_FORWARDING_VCL_FUNCTION(FUNC, COUNTER, 4, float)
#endif


namespace OpCounter {
  namespace impl {

    /* Mathematical functions */
    DEFINE_OPCOUNT_LANEWISE_FUNCTION(exp);
    DEFINE_OPCOUNT_LANEWISE_FUNCTION(sqrt);
    DEFINE_OPCOUNT_LANEWISE_FUNCTION(abs);
    DEFINE_OPCOUNT_LANEWISE_FUNCTION(round);
    DEFINE_OPCOUNT_LANEWISE_FUNCTION(floor);
    DEFINE_OPCOUNT_LANEWISE_FUNCTION(ceil);
    // missing: truncate (VCL: truncate, std: trunc)
    
    /* Comparative functions */
    DEFINE_OPCOUNT_COMPARATIVE_FUNCTION(min);
    DEFINE_OPCOUNT_COMPARATIVE_FUNCTION(max);

    /* Fuse multiply add functions */
    DEFINE_OPCOUNT_FMA_FUNCTION(mul_add, +, +);
    DEFINE_OPCOUNT_FMA_FUNCTION(mul_sub, +, -);
    DEFINE_OPCOUNT_FMA_FUNCTION(nmul_add, -, +);


    /* horizontal_add - So far the only intra register reduction */
    template<typename F, int size>
    OpCounter<F> horizontal_add(const OpCounterVector<F, size>& a)
    {
      BARRIER;
      return std::accumulate(a._v, a._v + size, OpCounter<F>(0.0));
    }

    /* select is somewhat special and does not match any other function as well */
    template<typename F, int size>
    OpCounterVector<F, size> select(const typename BooleanVector<F, size>::type& s,
				    const OpCounterVector<F, size>& a,
				    const OpCounterVector<F, size>& b)
    {
      BARRIER;
      OpCounterVector<F, size> r;
      for (std::size_t i=0; i<size; ++i)
        r._v[i] = s[i] ? a[i] : b[i];
      BARRIER;
      return r;
    }

    template<typename T, typename F, int size,
	         typename std::enable_if<std::is_convertible<T, F>::value, int>::type = 0>
    OpCounterVector<F, size> select(const typename BooleanVector<F, size>::type& s,
        const OpCounter<T>& a,
        const OpCounterVector<F, size>& b)
    {
      BARRIER;
      OpCounterVector<F, size> r;
      for (std::size_t i=0; i<size; ++i)
        r._v[i] = s[i] ? a : b[i];
      BARRIER;
      return r;
    }

    template<typename T, typename F, int size,
	         typename std::enable_if<std::is_convertible<T, F>::value, int>::type = 0>
    OpCounterVector<F, size> select(const typename BooleanVector<F, size>::type& s,
        const OpCounterVector<F,size>& a,
        const OpCounter<T>& b)
    {
      BARRIER;
      OpCounterVector<F, size> r;
      for (std::size_t i=0; i<size; ++i)
        r._v[i] = s[i] ? a[i] : b;
      BARRIER;
      return r;
    }

    template<int size>
    typename IntegerVector<double, size>::type truncate_to_int64(const OpCounterVector<double, size>& a)
    {
      return truncate_to_int64(to_original(a));
    }
  } // namespace impl
} // namespace OpCounter


/* Define those ugl functions from VCL that take a template parameter
 * per vector lane. Admittedly, the generic macro implementation is superugly.
 * This is mainly because of VCLs thing with mangling the vector type
 * into the function name with these... Also, the fact that these functions
 * expect explicit template parameters screws ADL and we pollute the global
 * namespace with these.
 */
DEFINE_OPCOUNT_TEMPLATED_FORWARDING_VCL_FUNCTION(blend, blends);
DEFINE_OPCOUNT_TEMPLATED_FORWARDING_VCL_FUNCTION(permute, permutes);

template<int n, typename IV, typename F>
OpCounter::impl::OpCounterVector<F, OpCounter::impl::VectorSize<IV>::size> lookup(const IV& index, OpCounter::OpCounter<F> const * table)
{
  auto r_ = VCL_NAMESPACE::lookup<n>(index, &(table[0]._v));
  return OpCounter::impl::to_opcounted<F, OpCounter::impl::VectorSize<IV>::size>(r_);
}

#endif
