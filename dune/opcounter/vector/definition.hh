#ifndef DUNE_OPCOUNTER_VECTOR_DEFINITION_HH
#define DUNE_OPCOUNTER_VECTOR_DEFINITION_HH

#include<algorithm>

#include<dune/opcounter/opcounter.hh>
#include<vectorclass/vectorclass.h>

#define BARRIER asm volatile("": : :"memory")

namespace OpCounter {
  namespace impl {

    template<typename F, int s>
    struct OpCounterVector
    {
      /* This is the actual storage array */
      OpCounter<F> _v[s];

      /* Default constructor */
      OpCounterVector() {}

      /* Constructor from a single scalar */
      OpCounterVector(OpCounter<F> a)
      {
        BARRIER;
        std::fill(_v, _v + s, a);
        BARRIER;
      }

      /* Constructor from a list of scalars of size 'size' */
      template<typename...T, typename std::enable_if<sizeof...(T) == s, int>::type = 0>
      OpCounterVector(T... args) : _v{args...}
      {}

      /* Constructor from a lower/upper half */
      OpCounterVector(const OpCounterVector<F, s/2>& low, const OpCounterVector<F, s/2>& high)
      {
	BARRIER;
	std::copy(low._v, low._v + s / 2, _v);
	std::copy(high._v, high._v + s / 2, _v + s / 2);
	BARRIER;
      }

      OpCounterVector& load(const OpCounter<F>* p)
      {
	BARRIER;
	std::copy(p, p + s, _v);
	BARRIER;
	return *this;
      }

      OpCounterVector& load_a(const OpCounter<F>* p)
      {
	return load(p);
      }

      void store(OpCounter<F>* p) const
      {
	BARRIER;
	std::copy(_v, _v + s, p);
	BARRIER;
      }

      void store_a(OpCounter<F>* p) const
      {
	store(p);
      }

      const OpCounterVector& insert(uint32_t index, OpCounter<F> value)
      {
	BARRIER;
	_v[index] = value;
	BARRIER;
	return *this;
      }

      OpCounter<F> extract(uint32_t index) const
      {
	BARRIER;
	return _v[index];
      }

      OpCounter<F> operator[](uint32_t index) const
      {
	return extract(index);
      }

      OpCounterVector<F, s / 2> get_low() const
      {
	BARRIER;
	OpCounterVector<F, s / 2> result;
	result.load(_v);
	BARRIER;
	return result;
      }

      OpCounterVector<F, s / 2> get_high() const
      {
	BARRIER;
	OpCounterVector<F, s / 2> result;
	result.load(_v + s / 2);
	BARRIER;
	return result;
      }

      constexpr static int size()
      {
	return s;
      }
    };

    /* Template specialization for trivial vector. Actually not sure whether
     * this works as intended, but its a corner case anyway...
     */
    template<typename F>
    struct OpCounterVector<F, 1> : public OpCounter<F>
    {
        OpCounterVector& load(const OpCounter<F>* p)
        {
            this->_v = p->_v;
            return *this;
        }
        
        OpCounterVector& load_a(const OpCounter<F>* p)
        {
            return load(p);
        }
    };

  } // namespace impl
} // namespace OpCounter

#endif
