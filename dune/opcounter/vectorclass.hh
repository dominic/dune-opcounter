#ifndef DUNE_OPCOUNTER_VECTORCLASS_HH
#define DUNE_OPCOUNTER_VECTORCLASS_HH

#ifdef VCL_NAMESPACE
#error Do not manually set VCL_NAMESPACE, it is used internally by dune-opcounter
#else
#define VCL_NAMESPACE _vcl
#endif

#include<vectorclass/vectorclass.h>

#include<dune/opcounter/vector/definition.hh>
#include<dune/opcounter/vector/traits.hh>
#include<dune/opcounter/vector/operators.hh>
#include<dune/opcounter/vector/functions.hh>


// 128 bits
using Vec2d = OpCounter::impl::OpCounterVector<double, 2>;
using Vec4f = OpCounter::impl::OpCounterVector<float, 4>;

#if MAX_VECTOR_SIZE >= 256
// 256 bits
using Vec4d = OpCounter::impl::OpCounterVector<double, 4>;
using Vec8f = OpCounter::impl::OpCounterVector<float, 8>;
#endif

#if MAX_VECTOR_SIZE >= 512
// 512 bits
using Vec8d = OpCounter::impl::OpCounterVector<double, 8>;
using Vec16f = OpCounter::impl::OpCounterVector<float, 16>;
#endif


#endif
