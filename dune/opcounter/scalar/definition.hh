#ifndef DUNE_OPCOUNTER_SCALAR_DEFINITION_HH
#define DUNE_OPCOUNTER_SCALAR_DEFINITION_HH

#ifndef DO_ATOMIC_COUNTER_INCREMENTS
#define DO_ATOMIC_COUNTER_INCREMENTS 0
#endif

#include<cstdlib>
#include<istream>
#include<ostream>
#include<string>
#include<atomic>

namespace OpCounter {

  template<typename F>
  class OpCounter
  {
    public:
    /* The underlying floating point value storage */
    F _v;

    /* This is the type that is used for the actual operation counts */
    using size_type = std::size_t;

    /* Default constructor */
    OpCounter()
      : _v()
    {}

    /* Construct from an instance of the underlying type */
    OpCounter(const F& f)
      : _v(f)
    {}

    /* Construct from an rvalue reference of the underlying type */
    OpCounter(F&& f)
      : _v(f)
    {}

    /* Construct from a char array (often used to define multiprecision literals) */
    explicit OpCounter(std::string s)
      : _v(std::stod(s))
    {}

    /* Cast operator to the underlying type -- EXPLICIT */
    explicit operator F() const
    {
      return _v;
    }

    OpCounter& operator=(const F& f)
    {
      _v = f;
      return *this;
    }

    OpCounter& operator=(F&& f)
    {
      _v = f;
      return *this;
    }

    /** ILU code form dune-istl has a cast to an integer! */
    explicit operator int() const
	{
      return (int) _v;
	}

    friend std::ostream& operator<<(std::ostream& os, const OpCounter& f)
    {
      os << "OC(" << f._v << ")";
      return os;
    }

    friend std::istream& operator>>(std::istream& is, OpCounter& f)
    {
      is >> f._v;
      return is;
    }

    // The alignment is necessary to work around
    // https://bugs.llvm.org/show_bug.cgi?id=34219:
    // "SLP vectorizer: aligned store to unaligned address"
    // The bug is present in 3.8.0 (tags/RELEASE_380/final 263969) and in
    // clang version 6.0.0 (trunk 310993)
    struct alignas(16) Counters
    {
#if DO_ATOMIC_COUNTER_INCREMENTS == 1
      std::atomic<size_type> addition_count;
      std::atomic<size_type> multiplication_count;
      std::atomic<size_type> division_count;
      std::atomic<size_type> exp_count;
      std::atomic<size_type> pow_count;
      std::atomic<size_type> sin_count;
      std::atomic<size_type> sqrt_count;
      std::atomic<size_type> comparison_count;
      std::atomic<size_type> blend_count;
      std::atomic<size_type> permute_count;
#else
      size_type addition_count;
      size_type multiplication_count;
      size_type division_count;
      size_type exp_count;
      size_type pow_count;
      size_type sin_count;
      size_type sqrt_count;
      size_type comparison_count;
      size_type blend_count;
      size_type permute_count;
#endif

      Counters()
	: addition_count(0)
	, multiplication_count(0)
	, division_count(0)
	, exp_count(0)
	, pow_count(0)
	, sin_count(0)
	, sqrt_count(0)
	, comparison_count(0)
	, blend_count(0)
	, permute_count(0)
      {}

      void reset()
      {
	addition_count = 0;
	multiplication_count = 0;
	division_count = 0;
	exp_count = 0;
	pow_count = 0;
	sin_count = 0;
	sqrt_count = 0;
	comparison_count = 0;
	blend_count = 0;
	permute_count = 0;
      }

      template<typename Stream>
      void reportOperations(Stream& os, std::string prefix, bool doReset = false)
      {
	auto total = addition_count + multiplication_count + division_count + exp_count + pow_count + sin_count + sqrt_count + comparison_count;
	if (total == 0)
	  return;
	os << prefix << " additions " << addition_count << std::endl
	   << prefix << " multiplications " << multiplication_count << std::endl
	   << prefix << " divisions " << division_count << std::endl
	   << prefix << " exp " << exp_count << std::endl
	   << prefix << " pow " << pow_count << std::endl
	   << prefix << " sin " << sin_count << std::endl
	   << prefix << " sqrt " << sqrt_count << std::endl
	   << prefix << " comparisons " << comparison_count << std::endl
	   << prefix << " blends " << blend_count << std::endl
	   << prefix << " permutes " << permute_count << std::endl
	   << prefix << " totalops " << total << std::endl;
	 if (doReset)
	   reset();
      }

      Counters& operator+=(const Counters& rhs)
      {
	addition_count += rhs.addition_count;
	multiplication_count += rhs.multiplication_count;
	division_count += rhs.division_count;
	exp_count += rhs.exp_count;
	pow_count += rhs.pow_count;
	sin_count += rhs.sin_count;
	sqrt_count += rhs.sqrt_count;
	comparison_count += rhs.comparison_count;
	blend_count += rhs.blend_count;
	permute_count += rhs.permute_count;
	return *this;
      }

      Counters operator-(const Counters& rhs)
      {
	Counters r;
	r.addition_count = addition_count - rhs.addition_count;
	r.multiplication_count = multiplication_count - rhs.multiplication_count;
	r.division_count = division_count - rhs.division_count;
	r.exp_count = exp_count - rhs.exp_count;
	r.pow_count = pow_count - rhs.pow_count;
	r.sin_count = sin_count - rhs.sin_count;
	r.sqrt_count = sqrt_count - rhs.sqrt_count;
	r.comparison_count = comparison_count - rhs.comparison_count;
	r.blend_count = blend_count - rhs.blend_count;
	r.permute_count = permute_count - rhs.permute_count;
	return r;
      }
    };

    static void additions(std::size_t n)
    {
      counters.addition_count += n;
    }

    static void multiplications(std::size_t n)
    {
      counters.multiplication_count += n;
    }

    static void divisions(std::size_t n)
    {
      counters.division_count += n;
    }

    static void comparisons(std::size_t n)
    {
      counters.comparison_count += n;
    }

    static void blends(std::size_t n)
    {
      counters.blend_count += n;
    }

    static void permutes(std::size_t n)
    {
      counters.permute_count += n;
    }

    static void reset()
    {
      counters.reset();
    }

    template<typename Stream>
    static void reportOperations(Stream& os, std::string prefix, bool doReset = false)
    {
      counters.reportOperations(os, prefix, doReset);
    }

    static Counters counters;
  };

  template<typename F>
  typename OpCounter<F>::Counters OpCounter<F>::counters;

} // namespace OpCounter


#endif
