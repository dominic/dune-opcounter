#ifndef DUNE_OPCOUNTER_SCALAR_OPERATORS_HH
#define DUNE_OPCOUNTER_SCALAR_OPERATORS_HH

#include<type_traits>

#include<dune/opcounter/scalar/definition.hh>

/* Define all the necessary operators for a given arithmetic operator */
#define DEFINE_OPCOUNT_ARITHMETIC_OPERATOR(OP, COUNTER)                           \
  /* left: OpCounter, right OpCounter */                                          \
  template<typename F>                                                            \
  OpCounter<F> operator OP(const OpCounter<F>& a, const OpCounter<F>& b)          \
  {                                                                               \
    OpCounter<F>::COUNTER(1);                                                     \
    return {a._v OP b._v};                                                        \
  }                                                                               \
                                                                                  \
  /* left: underlying floating point, right: OpCounter */                         \
  template<typename F>                                                            \
  OpCounter<F> operator OP(const F& a, const OpCounter<F>& b)                     \
  {                                                                               \
    OpCounter<F>::COUNTER(1);                                                     \
    return {a OP b._v};                                                           \
  }                                                                               \
                                                                                  \
  /* left: OpCounter, right: underlying floating point */                         \
  template<typename F>                                                            \
  OpCounter<F> operator OP(const OpCounter<F>& a, const F& b)                     \
  {                                                                               \
    OpCounter<F>::COUNTER(1);                                                     \
    return {a._v OP b};                                                           \
  }                                                                               \
                                                                                  \
  /* left: OpCounter, right: anything arithmetic from std (int etc.) */           \
  template<typename F, typename T>                                                \
  typename std::enable_if<std::is_arithmetic<T>::value, OpCounter<F>>::type       \
  operator OP(const OpCounter<F>& a, const T& b)                                  \
  {                                                                               \
    OpCounter<F>::COUNTER(1);                                                     \
    return {a._v OP b};                                                           \
  }                                                                               \
                                                                                  \
  /* left: anything arithmetic from std (int etc.), right: OpCounter */           \
  template<typename F, typename T>                                                \
  typename std::enable_if<std::is_arithmetic<T>::value, OpCounter<F>>::type       \
  operator OP(const T& a, const OpCounter<F>& b)                                  \
  {                                                                               \
    OpCounter<F>::COUNTER(1);                                                     \
    return {a OP b._v};                                                           \
  }                                                                               \
                                                                                  \
  /* accumulating operator, left: OpCounter, right: OpCounter types */            \
  template<typename F>                                                            \
  OpCounter<F>& operator OP##=(OpCounter<F>& a, const OpCounter<F>& b)            \
  {                                                                               \
    OpCounter<F>::COUNTER(1);                                                     \
    a._v OP##= b._v;                                                              \
    return a;                                                                     \
  }                                                                               \
                                                                                  \
  /* accumulating operator, left: OpCounter, right: underlying floating point */  \
  template<typename F>                                                            \
  OpCounter<F>& operator OP##=(OpCounter<F>& a, const F& b)                       \
  {                                                                               \
    OpCounter<F>::COUNTER(1);                                                     \
    a._v OP##= b;                                                                 \
    return a;                                                                     \
  }                                                                               \
                                                                                  \
  /* accumulating operator, left: OpCounter, right: anything arithmetic */        \
  template<typename F, typename T>                                                \
  typename std::enable_if<std::is_arithmetic<T>::value, OpCounter<F>&>::type      \
  operator OP##=(OpCounter<F>& a, const T& b)                                     \
  {                                                                               \
    OpCounter<F>::COUNTER(1);                                                     \
    a._v OP##= b;                                                                 \
    return a;                                                                     \
  }                                                                               \


/* Define all the necessary operators for a given logical operator */
#define DEFINE_OPCOUNT_LOGICAL_OPERATOR(OP)                                       \
  /* both operands are OpCounter types */                                         \
  template<typename F>                                                            \
  bool operator OP(const OpCounter<F>& a, const OpCounter<F>& b)                  \
  {                                                                               \
    OpCounter<F>::comparisons(1);                                                 \
    return a._v OP b._v;                                                          \
  }                                                                               \
                                                                                  \
  /* only left operand is an OpCounter type */                                    \
  template<typename F, typename T,                                                \
      typename std::enable_if<std::is_convertible<T, F>::value, int>::type = 0>   \
  bool operator OP(const T& a, const OpCounter<F>& b)                             \
  {                                                                               \
    OpCounter<F>::comparisons(1);                                                 \
    return a OP b._v;                                                             \
  }                                                                               \
                                                                                  \
  /* only right operand is an OpCounter type */                                   \
  template<typename F, typename T,                                                \
      typename std::enable_if<std::is_convertible<T, F>::value, int>::type = 0>   \
  bool operator OP(const OpCounter<F>& a, const T& b)                             \
  {                                                                               \
    OpCounter<F>::comparisons(1);                                                 \
    return a._v OP b;                                                             \
  }                                                                               \


#define DEFINE_OPCOUNT_UNARY_OPERATOR(OP, COUNT)                                  \
  template<typename F>                                                            \
  OpCounter<F> operator OP(const OpCounter<F>& a)                                 \
  {                                                                               \
    OpCounter<F>::additions(COUNT);                                               \
    return {OP a._v};                                                             \
  }                                                                               \


namespace OpCounter {

  // arithmetic operators
  DEFINE_OPCOUNT_ARITHMETIC_OPERATOR(+, additions);
  DEFINE_OPCOUNT_ARITHMETIC_OPERATOR(-, additions);
  DEFINE_OPCOUNT_ARITHMETIC_OPERATOR(*, multiplications);
  DEFINE_OPCOUNT_ARITHMETIC_OPERATOR(/, divisions);

  // logical operators
  DEFINE_OPCOUNT_LOGICAL_OPERATOR(<);
  DEFINE_OPCOUNT_LOGICAL_OPERATOR(<=);
  DEFINE_OPCOUNT_LOGICAL_OPERATOR(>);
  DEFINE_OPCOUNT_LOGICAL_OPERATOR(>=);
  DEFINE_OPCOUNT_LOGICAL_OPERATOR(!=);
  DEFINE_OPCOUNT_LOGICAL_OPERATOR(==);

  // unary operators
  DEFINE_OPCOUNT_UNARY_OPERATOR(+, 0);
  DEFINE_OPCOUNT_UNARY_OPERATOR(-, 1);

} // namespace OpCounter


#endif
