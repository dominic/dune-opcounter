#ifndef DUNE_OPCOUNTER_SCALAR_TRAITS_HH
#define DUNE_OPCOUNTER_SCALAR_TRAITS_HH

#include<type_traits>

#include<dune/opcounter/opcounter.hh>


// If this is used in a Dune context, we define a necessary
// type trait...
#if HAVE_DUNE_COMMON

#include<dune/common/typetraits.hh>

namespace Dune {

  template <typename T>
  struct IsNumber<OpCounter::OpCounter<T>>
    : public std::integral_constant<bool, IsNumber<T>::value>
  {};

}
#endif

namespace OpCounter {

  template<typename T>
  struct isOpCounter : public std::false_type
  {};

  template<typename F>
  struct isOpCounter<OpCounter<F>> : public std::true_type
  {};

  template<typename T>
  constexpr bool isOpCounterV = isOpCounter<T>::value;

} // namespace OpCounter

#endif
