#include <Catch2/single_include/catch.hpp>
#include <dune/opcounter/opcounter.hh>
#include <dune/opcounter/vectorclass.hh>
#include <vectorclass/vectorclass.h>
#include "test_impl.hh"

TEST_CASE("Testing constructors and definition functions for OpCounterVector")
{
    //construct from a single scalar
    OpCounter::OpCounter<double> test_opc = 1.0;
    OpCounter::impl::OpCounterVector<double, 4> test_opc_vector(test_opc);
    
    for( int i = 0; i < 4; i++ )
    {
        REQUIRE(test_opc_vector[i] == test_opc);
    }
    
    //construct from a list of scalars
    OpCounter::impl::OpCounterVector<double, 4> test_opc_vector2(0.0, 1.0, 2.0, 3.0);
    
    REQUIRE(test_opc_vector2[0] == 0.0);
    REQUIRE(test_opc_vector2[1] == 1.0);
    REQUIRE(test_opc_vector2[2] == 2.0);
    REQUIRE(test_opc_vector2[3] == 3.0);
    
    //construct from a list of opcounter scalars
    OpCounter::OpCounter<double> test_opc3 = 0.0;
    OpCounter::OpCounter<double> test_opc4 = 1.0;
    OpCounter::OpCounter<double> test_opc5 = 2.0;
    OpCounter::OpCounter<double> test_opc6 = 3.0;
    OpCounter::impl::OpCounterVector<double, 4> 
        test_opc_vector3(test_opc3, test_opc4, test_opc5, test_opc6);
    
    REQUIRE(test_opc_vector3[0] == 0.0);
    REQUIRE(test_opc_vector3[1] == 1.0);
    REQUIRE(test_opc_vector3[2] == 2.0);
    REQUIRE(test_opc_vector3[3] == 3.0);
    
    //testing the construct from lower and upper half function
    OpCounter::impl::OpCounterVector<double, 2> test_opc_vector5;
    test_opc_vector5[0] = 1.0;
    test_opc_vector5[1] = -1.0;
    
    OpCounter::impl::OpCounterVector<double, 4> test_opc_vector6(
        test_opc_vector5, test_opc_vector5);
    
    for( int i = 0; i < 4; i++ )
    {
        REQUIRE(test_opc_vector6[i] == test_opc_vector5[i%2]);
    }
    
    OpCounter::impl::OpCounterVector<double, 8> test_opc_vector7(
        test_opc_vector6, test_opc_vector6);
    
    for( int i = 0; i < 8; i++ )
    {
        REQUIRE(test_opc_vector7[i] == test_opc_vector6[i%4]);
    }
    
    REQUIRE(_vcl::horizontal_and(test_opc_vector7.get_low() == test_opc_vector6));
    REQUIRE(_vcl::horizontal_and(test_opc_vector7.get_high() == test_opc_vector6));
    REQUIRE(_vcl::horizontal_and(test_opc_vector6.get_low() == test_opc_vector5));
    REQUIRE(_vcl::horizontal_and(test_opc_vector6.get_high() == test_opc_vector5));
    
    //also for scalar OpCounters
    OpCounter::OpCounter<double> test_opc2 = 1.0;
    OpCounter::impl::OpCounterVector<double, 2> test_opc_vector8(
        test_opc2, test_opc2);
    
    for( int i = 0; i < 2; i++ )
    {
        REQUIRE(test_opc_vector8[i] == test_opc2);
    }
    
    REQUIRE(test_opc_vector8.get_low() == test_opc2);
    REQUIRE(test_opc_vector8.get_high() == test_opc2);
}
