#include <Catch2/single_include/catch.hpp>
#include <dune/opcounter/opcounter.hh>
#include <algorithm>
#include "test_impl.hh"

/* This test will ensure the correctness of the operation counter
 * (struct alignas(16) Counters). Thread safety will be dealt with
 * in another test.
 */
TEST_CASE("Testing the internal counter for scalar OpCounters (double)")
{
    //test operators
    test_impl_counter_binary<double>("double", "+",
                OpCounter::OpCounter<double>::counters.addition_count,
                [](auto a, auto b){return a + b;});
    test_impl_counter_binary<double>("double", "-",
                OpCounter::OpCounter<double>::counters.addition_count,
                [](auto a, auto b){return a - b;});
    test_impl_counter_binary<double>("double", "*",
                OpCounter::OpCounter<double>::counters.multiplication_count,
                [](auto a, auto b){return a * b;});
    test_impl_counter_binary<double>("double", "/",
                OpCounter::OpCounter<double>::counters.division_count,
                [](auto a, auto b){return a / b;});
    
    //test comparisons
    test_impl_counter_binary<double>("double", "<",
                OpCounter::OpCounter<double>::counters.comparison_count,
                [](auto a, auto b){return a < b;});
    test_impl_counter_binary<double>("double", "<=",
                OpCounter::OpCounter<double>::counters.comparison_count,
                [](auto a, auto b){return a <= b;});
    test_impl_counter_binary<double>("double", ">",
                OpCounter::OpCounter<double>::counters.comparison_count,
                [](auto a, auto b){return a > b;});
    test_impl_counter_binary<double>("double", ">=",
                OpCounter::OpCounter<double>::counters.comparison_count,
                [](auto a, auto b){return a >= b;});
    test_impl_counter_binary<double>("double", "!=",
                OpCounter::OpCounter<double>::counters.comparison_count,
                [](auto a, auto b){return a != b;});
    test_impl_counter_binary<double>("double", "==",
                OpCounter::OpCounter<double>::counters.comparison_count,
                [](auto a, auto b){return a == b;});
    
    //test mathematical functions
    test_impl_counter_unary<double>("double", "sin",
                OpCounter::OpCounter<double>::counters.sin_count,
                [](auto a){using std::sin; return sin(a);});
    test_impl_counter_unary<double>("double", "cos",
                OpCounter::OpCounter<double>::counters.sin_count,
                [](auto a){using std::cos; return cos(a);});
    test_impl_counter_unary<double>("double", "exp",
                OpCounter::OpCounter<double>::counters.exp_count,
                [](auto a){using std::exp; return exp(a);});
    test_impl_counter_unary<double>("double", "sqrt",
                OpCounter::OpCounter<double>::counters.sqrt_count,
                [](auto a){using std::sqrt; return sqrt(a);});
    test_impl_counter_binary_only_opc<double>("double", "pow",
                OpCounter::OpCounter<double>::counters.pow_count,
                [](auto a, auto b){using std::pow; return pow(a,b);});
    
    //test comparative functions
    test_impl_counter_unary<double>("double", "abs",
                OpCounter::OpCounter<double>::counters.comparison_count,
                [](auto a){using std::abs; return abs(a);});
    test_impl_counter_binary_only_opc<double>("double", "max",
                OpCounter::OpCounter<double>::counters.comparison_count,
                [](auto a, auto b){using std::max; return max(a,b);});
    test_impl_counter_binary_only_opc<double>("double", "min",
                OpCounter::OpCounter<double>::counters.comparison_count,
                [](auto a, auto b){using std::min; return min(a,b);});
    
    
    SECTION("Test operators on the struct Counters")
    {
        OpCounter::OpCounter<double>::Counters test_counters1;
        OpCounter::OpCounter<double>::Counters test_counters2;
        
        test_counters1.addition_count += 10;
        test_counters1.sin_count += 100;
        test_counters1.comparison_count += 1000;
        
        test_counters2 += test_counters1;
        
        REQUIRE(test_counters2.addition_count == 10);
        REQUIRE(test_counters2.sin_count == 100);
        REQUIRE(test_counters2.comparison_count == 1000);
        
        test_counters1.comparison_count = 999;
        
        test_counters2 = test_counters2 - test_counters1;
        
        REQUIRE(test_counters2.addition_count == 0);
        REQUIRE(test_counters2.sin_count == 0);
        REQUIRE(test_counters2.comparison_count == 1);
        
        test_counters1.reset();
        
        REQUIRE(test_counters1.addition_count == 0);
        REQUIRE(test_counters1.sin_count == 0);
        REQUIRE(test_counters1.comparison_count == 0);
    }
}
    
    
