#include <Catch2/single_include/catch.hpp>
#include <dune/opcounter/opcounter.hh>
#include <dune/opcounter/vectorclass.hh>
#include <vectorclass/vectorclass.h>
#include <vectorclass/vectormath_exp.h>
#include "test_impl.hh"

template<typename T, int size>
void vector_tests_functions(std::string vector_name, 
            std::vector<OpCounter::impl::OpCounterVector<T, size>> test_values,
            std::vector<T> test_values_scalar)
{
    // Operators to test:
    // exp, sqrt, abs, round, floor, ceil, min, max
    vector_test_impl_unary<T, size>(vector_name, "exp", test_values,
                            [](auto a){using _vcl::exp; return exp(a);});
    vector_test_impl_unary<T, size>(vector_name, "sqrt", test_values,
                            [](auto a){using _vcl::sqrt; return sqrt(a);});
    vector_test_impl_unary<T, size>(vector_name, "abs", test_values,
                            [](auto a){using _vcl::abs; return abs(a);});
    vector_test_impl_unary<T, size>(vector_name, "round", test_values,
                            [](auto a){using _vcl::round; return round(a);});
    vector_test_impl_unary<T, size>(vector_name, "floor", test_values,
                            [](auto a){using _vcl::floor; return floor(a);});
    vector_test_impl_unary<T, size>(vector_name, "ceil", test_values,
                            [](auto a){using _vcl::ceil; return ceil(a);});
    
    vector_test_impl_binary_num<T, size>(vector_name, "max",
                            test_values, test_values_scalar,
                            [](auto a, auto b){return max(a, b);});
    vector_test_impl_binary_num<T, size>(vector_name, "min", 
                            test_values, test_values_scalar,
                            [](auto a, auto b){return min(a, b);});
        
    //FMA tests: mul_add, mul_sub, nmul_add
    vector_test_impl_fma<T, size>(vector_name, "mul_add", 
                            test_values, test_values_scalar,
                            [](auto a, auto b, auto c){return mul_add(a,b,c);},
                            [](auto a, auto b, auto c){return a * b + c;});
    vector_test_impl_fma<T, size>(vector_name, "mul_sub", 
                            test_values, test_values_scalar,
                            [](auto a, auto b, auto c){return mul_sub(a,b,c);},
                            [](auto a, auto b, auto c){return a * b - c;});
    vector_test_impl_fma<T, size>(vector_name, "nmul_add", 
                            test_values, test_values_scalar,
                            [](auto a, auto b, auto c){return nmul_add(a,b,c);},
                            [](auto a, auto b, auto c){return -a * b + c;});
                          
        
    vector_test_impl_h_add<T, size>(vector_name, "horizontal_add", test_values);
        
    //TODO: select
}

TEST_CASE("Testing arithmetic functions of OpCounterVector")
{
    //size of test_values vectors
    const int test_value_size = 5;
    
    //scalar test values for double and float (used in all vector tests)
    std::vector<double> test_values_double(test_value_size);
    test_values_double[0] = 0;
    test_values_double[1] = -1;
    test_values_double[2] = 2.718281828459045;
    test_values_double[3] = 0.1e-308;
    test_values_double[4] = 1.7e308;
    
    std::vector<float> test_values_float(test_value_size);
    test_values_float[0] = 0;
    test_values_float[1] = -1;
    test_values_float[2] = 2.718281828459045;
    test_values_float[3] = 0.1e-38;
    test_values_float[4] = 1.7e38;
    
    //vectorclass types selected for testing:
    //Vec4f, Vec2d, Vec16f, Vec8d
    std::vector<Vec2d> test_values2d(test_value_size);
    test_values2d[0] = Vec2d(0.0, 0.0);
    test_values2d[1] = Vec2d(1.0, -1.0);
    test_values2d[2] = Vec2d(2.718281828459045, 3.1415926535897);
    test_values2d[3] = Vec2d(0.1e-308, -1.7e308);
    test_values2d[4] = Vec2d(1.7e308, 1.7e308);
    
    vector_tests_functions<double, 2>("double, 2", test_values2d, test_values_double);
    
    
    std::vector<Vec4f> test_values4f(test_value_size);
    test_values4f[0] = Vec4f(0.0, 0.0, 0.0, 0.0);
    test_values4f[1] = Vec4f(1.0, -1.0, 10.0, -10.0);
    test_values4f[2] = Vec4f(2.718281828459045, 3.1415926535897, 
                             -2.718281828459045, -3.1415926535897);
    test_values4f[3] = Vec4f(-1.7e38, 0.0, 0.0, 0.1e-38);
    test_values4f[4] = Vec4f(1.7e38, 1.7e38, 1.7e38, 1.7e38);
    
    vector_tests_functions<float, 4>("float, 4", test_values4f, test_values_float);
    
#if MAX_VECTOR_SIZE >= 256
    std::vector<Vec4d> test_values4d(test_value_size);
    test_values4d[0] = Vec4d(0.0, 0.0, 0.0, 0.0);
    test_values4d[1] = Vec4d(1.0, -1.0, 99999.0, -99999.0);
    test_values4d[2] = Vec4d(2.718281828459045, 3.1415926535897, 6.62607004081e-34, 
                             6.02214085774e23);
    test_values4d[3] = Vec4d(1.0, 1.0e80, 1.0e160,  1.0e240);
    test_values4d[4] = Vec4d(1.7e308, -1.7e308, 0.1e-308, -0.1e-308);
    
    vector_tests_functions<double, 4>("double, 4", test_values4d, test_values_double);
    
    
    std::vector<Vec8f> test_values8f(test_value_size);
    test_values8f[0] = Vec8f(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
    test_values8f[1] = Vec8f(1.0, -1.0, 10.0, -10.0, 999.0, -999.0, 99999.0, -99999.0);
    test_values8f[2] = Vec8f(2.718281828459045, 3.1415926535897, -2.718281828459045,
                             -3.1415926535897, 6.62607004081e-34, -6.62607004081e-34, 
                             6.02214085774e23, -6.02214085774e23);
    test_values8f[3] = Vec8f(1.0, 1.0e4, 1.0e8, 1.0e12, 
                             1.0e16, 1.0e20, 1.0e24, 1.0e28);
    test_values8f[4] = Vec8f(1.7e38, -1.7e38, 0.1e-38, -0.1e-38, 
                             0.0, 0.0, 0.0, 0.0);
    
    vector_tests_functions<float, 8>("float, 8", test_values8f, test_values_float);
#endif
    
#if MAX_VECTOR_SIZE >= 512
    std::vector<Vec8d> test_values8d(test_value_size);
    test_values8d[0] = Vec8d(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
    test_values8d[1] = Vec8d(1.0, -1.0, 10.0, -10.0, 999.0, -999.0, 99999.0, -99999.0);
    test_values8d[2] = Vec8d(2.718281828459045, 3.1415926535897, -2.718281828459045,
                             -3.1415926535897, 6.62607004081e-34, -6.62607004081e-34, 
                             6.02214085774e23, -6.02214085774e23);
    test_values8d[3] = Vec8d(1.0, 1.0e40, 1.0e80, 1.0e120, 
                             1.0e160, 1.0e200, 1.0e240, 1.0e280);
    test_values8d[4] = Vec8d(1.7e308, -1.7e308, 0.1e-308, -0.1e-308, 
                             0.0, 0.0, 0.0, 0.0);
    
    vector_tests_functions<double, 8>("double, 8", test_values8d, test_values_double);
    
    
    std::vector<Vec16f> test_values16f(test_value_size);
    test_values16f[0] = Vec16f(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                               0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
    test_values16f[1] = Vec16f(1.0, -1.0, 10.0, -10.0, 999.0, -999.0, 99999.0, 
                               -99999.0, 1.0, -1.0, 10.0, -10.0, 999.0, -999.0, 
                               99999.0, -99999.0);
    test_values16f[2] = Vec16f(2.718281828459045, 3.1415926535897, -2.718281828459045,
                             -3.1415926535897, 6.62607004081e-34, -6.62607004081e-34, 
                             6.02214085774e23, -6.02214085774e23, 2.718281828459045,
                             3.1415926535897, -2.718281828459045, -3.1415926535897,
                             6.62607004081e-34, -6.62607004081e-34, 6.02214085774e23,
                             -6.02214085774e23);
    test_values16f[3] = Vec16f(1.0, 1.0e4, 1.0e8, 1.0e12, 1.0e16, 1.0e20, 1.0e24, 
                               1.0e28, 1.0, 1.0e4, 1.0e8, 1.0e12, 1.0e16, 1.0e20, 
                               1.0e24, 1.0e28);
    test_values16f[4] = Vec16f(1.7e38, -1.7e38, 0.1e-38, -0.1e-38, 
                               0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                               1.7e38, -1.7e38, 0.1e-38, -0.1e-38);
    
    vector_tests_functions<float, 16>("float, 16", test_values16f, test_values_float);
#endif
}
