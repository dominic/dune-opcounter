#include <Catch2/single_include/catch.hpp>
#include <dune/opcounter/opcounter.hh>
#include <thread>

/* Since thread safety is not implemented for OpCounters at this point,
 * this test is designed to fail (for now). The 'mayfail' tag will make
 * sure this does not cause the whole test suite to fail, but will rather
 * show us an estimate on how bad the thread safety problem is and how
 * much progress were making when fixing it.
 */

// simple function to call from the threads
void minimal_thread_test(int num_operations)
{
    for( int i = 0; i < num_operations; i++ )
    {
        OpCounter::OpCounter<double> opc = 1.;
    
        opc + 1.;
    }
}

TEST_CASE("Testing thread safety for the internal counter", "[!mayfail]")
{
    // set the amount of threads to be started
    const int max_threads = 16;
    
    // set the amount of operations to be done per thread
    const int num_operations = 10000;
    
    for(int num_threads = 1; num_threads <= max_threads; 
        num_threads = num_threads*2)
    {
        SECTION("Testing thread safety for " + 
            std::to_string(num_threads) + " threads.")
        {
            OpCounter::OpCounter<double>::counters.reset();
    
            std::thread threads[num_threads];
    
            for( int i = 0; i < num_threads; i++ )
            {
                threads[i] = std::thread(minimal_thread_test, num_operations);
            }
    
            for( int i = 0; i < num_threads; i++ )
            {
                threads[i].join();
            }
    
            REQUIRE(num_threads*num_operations == 
                OpCounter::OpCounter<double>::counters.addition_count);
        }
    }
}
