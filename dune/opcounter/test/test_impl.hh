#include <Catch2/single_include/catch.hpp>
#include <dune/opcounter/opcounter.hh>
#include <dune/opcounter/vectorclass.hh>
#include <limits>
#include <iostream>
#include <cmath>

/** Replacement for the '==' operator to ensure tests do not fail due to
 * differences below machine precision and different types of NaN or inf 
 */
template<typename T>
bool almost_equals(T value1, T value2)
{
    
    if((std::isnan(value1) && std::isnan(value2)) 
        || (std::isinf(value1) && std::isinf(value2)))
    {
        return true;
    }
    else
    {
        return std::abs(value1 - value2) <= std::numeric_limits<T>::epsilon()
            * std::abs(value1 + value2);
    }
}

/** almost_equals for vectors
 */
template<typename T, int size>
bool almost_equals_vec(typename OpCounter::impl::OriginalVector<T, size>::type value1,
                       typename OpCounter::impl::OriginalVector<T, size>::type value2)
{
    bool result = true;
    for( std::size_t i = 0; i < size; i++ )
    {
        result = result && almost_equals<T>(value1[i], value2[i]);
    }
    return result;
}

/** Summarizes the given vectors into a string to give further information
 * about a test's failure.
 * \param result1 First result
 * \param result2 Second result
 */
template<typename T, int size, typename R1, typename R2>
std::string summarize_failure(const R1& result1, const R2& result2)
{
    std::string output = "";
    
    output += "First result:\n";
    
    for( std::size_t i = 0; i < size; i++ )
    {
        output += std::to_string(result1[i]) + "\n";
    }
    
    output += "Second result:\n";
    
    for( std::size_t i = 0; i < size; i++ )
    {
        output += std::to_string(result2[i]) + "\n";
    }
    
    return output;
}

/** Implementation for tests of unary operators and functions of datatype T.
 * \param data_name datatype name as string, used for test section naming
 * \param op_name operator name as string, used for test section naming
 * \param test_values a std::vector of values of the datatype to be tested
 * \param op the operator to be tested, to be input as a lambda
 */
template<typename T, typename OP>
void test_impl_unary(std::string data_name, std::string op_name, 
                     std::vector<T>& test_values, OP op)
{
    SECTION("Testing " + op_name + " for OpCounter<" + data_name + ">")
    {
        for( std::size_t i = 0; i < test_values.size(); i++ )
        {
            REQUIRE(almost_equals(op(OpCounter::OpCounter<T>(
                test_values[i]))._v, op(test_values[i])));
        }
    }
}


/** Implementation for tests of arithmetical binary operators and functions 
 *  of datatype T.
 * \param data_name datatype name as string, used for test section naming
 * \param op_name operator name as string, used for test section naming
 * \param test_values a std::vector of values of the datatype to be tested
 * \param op the operator to be tested, to be input as a lambda
 */
template<typename T, typename OP>
void test_impl_binary_num(std::string data_name, std::string op_name, 
                      std::vector<T>& test_values, OP op)
{
    SECTION("Testing " + op_name + " for OpCounter<" + data_name +  
        " - OpCounter<" + data_name + ">")
    {
        for( std::size_t i = 0; i < test_values.size(); i++ )
        {
            for( std::size_t j = i + 1; j < test_values.size(); j++ )
            {
                REQUIRE(almost_equals(op(OpCounter::OpCounter<T>(
                    test_values[i]), OpCounter::OpCounter<T>(test_values[j]))._v,
                    op(test_values[i], test_values[j])));
            }
        }
    }
    
    SECTION("Testing " + op_name + " for " + data_name + 
        " - OpCounter<" + data_name + ">")
    {
        for( std::size_t i = 0; i < test_values.size(); i++ )
        {
            for( std::size_t j = i + 1; j < test_values.size(); j++ )
            {
                REQUIRE(almost_equals(op(OpCounter::OpCounter<T>(
                    test_values[i]), test_values[j])._v, 
                    op(test_values[i], test_values[j])));
            }
        }
    }
    
    SECTION("Testing " + op_name + " for OpCounter<" + data_name + 
        "> - " + data_name)
    {
        for( std::size_t i = 0; i < test_values.size(); i++ )
        {
            for( std::size_t j = i + 1; j < test_values.size(); j++ )
            {
                REQUIRE(almost_equals(op(test_values[i], 
                    OpCounter::OpCounter<T>(test_values[j]))._v, 
                    op(test_values[i], test_values[j])));
            }
        }
    }
}

/** Implementation for tests of comparative binary operators and functions 
 *  of datatype T.
 * \param data_name datatype name as string, used for test section naming
 * \param op_name operator name as string, used for test section naming
 * \param test_values a std::vector of values of the datatype to be tested
 * \param op the operator to be tested, to be input as a lambda
 */
template<typename T, typename OP>
void test_impl_binary_com(std::string data_name, std::string op_name, 
                      std::vector<T>& test_values, OP op)
{
    SECTION("Testing " + op_name + " for OpCounter<" + data_name +  
        " - OpCounter<" + data_name + ">")
    {
        for( std::size_t i = 0; i < test_values.size(); i++ )
        {
            for( std::size_t j = i + 1; j < test_values.size(); j++ )
            {
                REQUIRE(op(OpCounter::OpCounter<T>(
                    test_values[i]), OpCounter::OpCounter<T>(test_values[j])) ==
                    op(test_values[i], test_values[j]));
            }
        }
    }
    
    SECTION("Testing " + op_name + " for " + data_name + 
        " - OpCounter<" + data_name + ">")
    {
        for( std::size_t i = 0; i < test_values.size(); i++ )
        {
            for( std::size_t j = i + 1; j < test_values.size(); j++ )
            {
                REQUIRE(op(OpCounter::OpCounter<T>(
                    test_values[i]), test_values[j]) == 
                    op(test_values[i], test_values[j]));
            }
        }
    }
    
    SECTION("Testing " + op_name + " for OpCounter<" + data_name + 
        "> - " + data_name)
    {
        for( std::size_t i = 0; i < test_values.size(); i++ )
        {
            for( std::size_t j = i + 1; j < test_values.size(); j++ )
            {
                REQUIRE(op(test_values[i], 
                    OpCounter::OpCounter<T>(test_values[j])) == 
                    op(test_values[i], test_values[j]));
            }
        }
    }
}

/** Implementation for tests of binary operators and functions of datatype T,
 *  which are not defined for OpCounter<T>-T or vice versa. (pow)
 * \param data_name datatype name as string, used for test section naming
 * \param op_name operator name as string, used for test section naming
 * \param test_values a std::vector of values of the datatype to be tested
 * \param op the operator to be tested, to be input as a lambda
 */
template<typename T, typename OP>
void test_impl_binary_only_opc_num(std::string data_name, std::string op_name, 
                      std::vector<T>& test_values, OP op)
{
    SECTION("Testing " + op_name + " for OpCounter<" + data_name +  
        " - OpCounter<" + data_name + ">")
    {
        for( std::size_t i = 0; i < test_values.size(); i++ )
        {
            for( std::size_t j = i + 1; j < test_values.size(); j++ )
            {
                REQUIRE(almost_equals(op(OpCounter::OpCounter<T>(
                    test_values[i]), OpCounter::OpCounter<T>(test_values[j]))._v,
                    op(test_values[i], test_values[j])));
            }
        }
    }
}

/** Implementation for tests of comparative binary operators and functions 
 *  of datatype T, which are not defined for OpCounter<T>-T or vice versa. 
 *  (max, min)
 * \param data_name datatype name as string, used for test section naming
 * \param op_name operator name as string, used for test section naming
 * \param test_values a std::vector of values of the datatype to be tested
 * \param op the operator to be tested, to be input as a lambda
 */
template<typename T, typename OP>
void test_impl_binary_only_opc_com(std::string data_name, std::string op_name, 
                      std::vector<T>& test_values, OP op)
{
    SECTION("Testing " + op_name + " for OpCounter<" + data_name +  
        " - OpCounter<" + data_name + ">")
    {
        for( std::size_t i = 0; i < test_values.size(); i++ )
        {
            for( std::size_t j = i + 1; j < test_values.size(); j++ )
            {
                REQUIRE(op(OpCounter::OpCounter<T>(
                    test_values[i]), OpCounter::OpCounter<T>(test_values[j])) ==
                    op(test_values[i], test_values[j]));
            }
        }
    }
}

/** Implementation for tests of the internal operation counter (binary operations)
 * \param data_name datatype name as string, used for test section naming
 * \param op_name operator name as string, used for test section naming
 * \param counter pointer to the internal counter specific to the operation
 * \param op the operator to be tested, to be input as a lambda
 */
template<typename T, typename OP>
void test_impl_counter_binary(std::string data_name, std::string op_name, 
                      std::size_t &counter, OP op)
{
    SECTION("Testing the internal counter for the operation " + op_name + 
        " for OpCounter<" + data_name + ">")
    {
        std::size_t prev_counter = counter;
        op(OpCounter::OpCounter<T>(2.0), OpCounter::OpCounter<T>(2.0));
        REQUIRE(counter == prev_counter + 1);
        
        prev_counter = counter;
        op(OpCounter::OpCounter<T>(2.0), T(2.0));
        REQUIRE(counter == prev_counter + 1);
        
        prev_counter = counter;
        op(T(2.0), OpCounter::OpCounter<T>(2.0));
        REQUIRE(counter == prev_counter + 1);
    }
}

/** Implementation for tests of the internal operation counter (unary operations)
 * \param data_name datatype name as string, used for test section naming
 * \param op_name operator name as string, used for test section naming
 * \param counter pointer to the internal counter specific to the operation
 * \param op the operator to be tested, to be input as a lambda
 */
template<typename T, typename OP>
void test_impl_counter_unary(std::string data_name, std::string op_name, 
                      std::size_t &counter, OP op)
{
    SECTION("Testing the internal counter for the operation " + op_name + 
        " for OpCounter<" + data_name + ">")
    {
        std::size_t prev_counter = counter;
        op(OpCounter::OpCounter<T>(2.0));
        REQUIRE(counter == prev_counter + 1);
    }
}

/** Implementation for tests of the internal operation counter
 *  which are not defined for OpCounter<T>-T or vice versa. (max, min, pow)
 * \param data_name datatype name as string, used for test section naming
 * \param op_name operator name as string, used for test section naming
 * \param counter pointer to the internal counter specific to the operation
 * \param op the operator to be tested, to be input as a lambda
 */
template<typename T, typename OP>
void test_impl_counter_binary_only_opc(std::string data_name, std::string op_name, 
                      std::size_t &counter, OP op)
{
    SECTION("Testing the internal counter for the operation " + op_name + 
        " for OpCounter<" + data_name + ">")
    {
        std::size_t prev_counter = counter;
        op(OpCounter::OpCounter<T>(2.0), OpCounter::OpCounter<T>(2.0));
        REQUIRE(counter == prev_counter + 1);
    }
}

/** Implementation for tests of unary operators and functions of vectors of
 *  data type T and size size.
 * \param data_name datatype name as string, used for test section naming
 * \param op_name operator name as string, used for test section naming
 * \param test_values a std::vector of vectors of the datatype to be tested
 * \param op the operator to be tested, to be input as a lambda
 */
template<typename T, int size, typename OP>
void vector_test_impl_unary(std::string data_name, std::string op_name, 
            std::vector<OpCounter::impl::OpCounterVector<T, size>>& test_values, 
            OP op)
{
    SECTION("Testing " + op_name + " for OpCounterVector<" + data_name + ">")
    {
        for( std::size_t i = 0; i < test_values.size(); i++ )
        {
            std::string error_message = summarize_failure<T, size>(
                OpCounter::impl::to_original(op(test_values[i])),
                op(OpCounter::impl::to_original(test_values[i])));
            INFO(error_message);
            
            REQUIRE(almost_equals_vec<T, size>(OpCounter::impl::to_original(op(test_values[i])),
                                     op(OpCounter::impl::to_original(test_values[i]))));
        }
    }
}

/** Implementation for tests of arithmetical binary operators and functions 
 *  of vectors of data type T and size size.
 * \param data_name datatype name as string, used for test section naming
 * \param op_name operator name as string, used for test section naming
 * \param test_values a std::vector of vectors of the datatype to be tested
 * \param test_values_scalar a std::vector of values of the datatype to be tested
 * \param op the operator to be tested, to be input as a lambda
 */
template<typename T, int size, typename OP>
void vector_test_impl_binary_num(std::string data_name, std::string op_name, 
            std::vector<OpCounter::impl::OpCounterVector<T, size>>& test_values, 
            std::vector<T>& test_values_scalar, OP op)
{
    SECTION("Testing " + op_name + " for OpCounterVector<" + data_name +  
        " - OpCounterVector<" + data_name + ">")
    {
        for( std::size_t i = 0; i < test_values.size(); i++ )
        {
            for( std::size_t j = i + 1; j < test_values.size(); j++ )
            {
                std::string error_message = summarize_failure<T, size>(
                OpCounter::impl::to_original(op(test_values[i], test_values[j])),
                op(OpCounter::impl::to_original(test_values[i]), OpCounter::impl::to_original(test_values[j])));
                INFO(error_message);
                
                REQUIRE(almost_equals_vec<T, size>(OpCounter::impl::to_original(
                    op(test_values[i], test_values[j])), 
                    op(OpCounter::impl::to_original(test_values[i]),
                    OpCounter::impl::to_original(test_values[j]))));
            }
        }
    }
    
    SECTION("Testing " + op_name + " for " + data_name + 
        " - OpCounter<" + data_name + ">")
    {
        for( std::size_t i = 0; i < test_values_scalar.size(); i++ )
        {
            for( std::size_t j = i + 1; j < test_values.size(); j++ )
            {
                std::string error_message = summarize_failure<T, size>(
                    OpCounter::impl::to_original(op(
                    OpCounter::OpCounter<T>(test_values_scalar[i]), 
                    test_values[j])), op(test_values_scalar[i], 
                    OpCounter::impl::to_original(test_values[j])));
                INFO(error_message);
                
                REQUIRE(almost_equals_vec<T, size>(OpCounter::impl::to_original(op(
                    OpCounter::OpCounter<T>(test_values_scalar[i]), 
                    test_values[j])), op(test_values_scalar[i], 
                    OpCounter::impl::to_original(test_values[j]))));
            }
        }
    }
    
    SECTION("Testing " + op_name + " for OpCounter<" + data_name + 
        "> - " + data_name)
    {
        for( std::size_t i = 0; i < test_values.size(); i++ )
        {
            for( std::size_t j = i + 1; j < test_values_scalar.size(); j++ )
            {
                std::string error_message = summarize_failure<T, size>(
                    OpCounter::impl::to_original(op(
                    test_values[j], OpCounter::OpCounter<T>(
                    test_values_scalar[i]))), op(OpCounter::impl::to_original(
                    test_values[j]), test_values_scalar[i]));
                INFO(error_message);
                
                REQUIRE(almost_equals_vec<T, size>(OpCounter::impl::to_original(op(
                    test_values[j], OpCounter::OpCounter<T>(
                    test_values_scalar[i]))), op(OpCounter::impl::to_original(
                    test_values[j]), test_values_scalar[i])));
            }
        }
    }
}

/** Implementation for tests of comparative binary operators and functions 
 *  of vectors of data type T and size size.
 * \param data_name datatype name as string, used for test section naming
 * \param op_name operator name as string, used for test section naming
 * \param test_values a std::vector of vectors of the datatype to be tested
 * \param test_values_scalar a std::vector of values of the datatype to be tested
 * \param op the operator to be tested, to be input as a lambda
 */
template<typename T, int size, typename OP>
void vector_test_impl_binary_com(std::string data_name, std::string op_name, 
            std::vector<OpCounter::impl::OpCounterVector<T, size>>& test_values, 
            std::vector<T>& test_values_scalar, OP op)
{
    SECTION("Testing " + op_name + " for OpCounterVector<" + data_name +  
        " - OpCounterVector<" + data_name + ">")
    {
        for( std::size_t i = 0; i < test_values.size(); i++ )
        {
            for( std::size_t j = i + 1; j < test_values.size(); j++ )
            {
                std::string error_message = summarize_failure<T, size>(
                    op(test_values[i], test_values[j]), 
                    op(OpCounter::impl::to_original(test_values[i]), OpCounter::impl::to_original(test_values[j])));
                INFO(error_message);
                
                REQUIRE(_vcl::horizontal_and(
                    op(test_values[i], test_values[j]) == 
                    op(OpCounter::impl::to_original(test_values[i]),
                    OpCounter::impl::to_original(test_values[j]))));
            }
        }
    }
    
    SECTION("Testing " + op_name + " for " + data_name + 
        " - OpCounter<" + data_name + ">")
    {
        for( std::size_t i = 0; i < test_values_scalar.size(); i++ )
        {
            for( std::size_t j = i + 1; j < test_values.size(); j++ )
            {
                std::string error_message = summarize_failure<T, size>(
                    op(OpCounter::OpCounter<T>(test_values_scalar[i]), 
                    test_values[j]), op(test_values_scalar[i], 
                    OpCounter::impl::to_original(test_values[j])));
                INFO(error_message);
                
                REQUIRE(_vcl::horizontal_and(op(
                    OpCounter::OpCounter<T>(test_values_scalar[i]), 
                    test_values[j]) == op(test_values_scalar[i], 
                    OpCounter::impl::to_original(test_values[j]))));
            }
        }
    }
    
    SECTION("Testing " + op_name + " for OpCounter<" + data_name + 
        "> - " + data_name)
    {
        for( std::size_t i = 0; i < test_values.size(); i++ )
        {
            for( std::size_t j = i + 1; j < test_values_scalar.size(); j++ )
            {
                std::string error_message = summarize_failure<T, size>(
                    op(test_values[j], OpCounter::OpCounter<T>(
                    test_values_scalar[i])), op(OpCounter::impl::to_original(
                    test_values[j]), test_values_scalar[i]));
                INFO(error_message);
                
                REQUIRE(_vcl::horizontal_and(op(
                    test_values[j], OpCounter::OpCounter<T>(
                    test_values_scalar[i])) == op(OpCounter::impl::to_original(
                    test_values[j]), test_values_scalar[i])));
            }
        }
    }
}

/** Implementation for tests of FMA operations on vector and scalar combinations.
 * \param data_name datatype name as string, used for test section naming
 * \param op_name operator name as string, used for test section naming
 * \param test_values a std::vector of vectors of the datatype to be tested
 * \param test_values_scalar a std::vector of values of the datatype to be tested
 * \param fma_op the FMA operator to be tested, to be input as a lambda
 * \param op the operator to be tested, to be input as a lambda
 */
template<typename T, int size, typename FMA_OP, typename OP>
void vector_test_impl_fma(std::string data_name, std::string op_name, 
            std::vector<OpCounter::impl::OpCounterVector<T, size>>& test_values,
            std::vector<T>& test_values_scalar, FMA_OP fma_op, OP op)
{
    SECTION("Testing FMA operation " + op_name + " for OpCounterVector<" 
            + data_name + ">")
    {
        //vector - vector - vector
        for( std::size_t i = 0; i < test_values.size(); i++ )
        {
            for( std::size_t j = 0; j < test_values.size(); j++ )
            {
                for( std::size_t k = 0; k < test_values.size(); k++ )
                {
                    OpCounter::impl::OpCounterVector<T, size> test1 = fma_op(
                        test_values[i], test_values[j], test_values[k]);
                    
                    OpCounter::impl::OpCounterVector<T, size> test2;
                    for( std::size_t l = 0; l < size; l++ )
                    {
                        test2._v[l] = op(test_values[i]._v[l], 
                                      test_values[j]._v[l], 
                                      test_values[k]._v[l]);
                    }
                        
                    REQUIRE(almost_equals_vec<T, size>(
                        OpCounter::impl::to_original(test1), 
                        OpCounter::impl::to_original(test2)));
                }
            }
        }
        
        //vector - vector - scalar
        for( std::size_t i = 0; i < test_values.size(); i++ )
        {
            for( std::size_t j = 0; j < test_values.size(); j++ )
            {
                for( std::size_t k = 0; k < test_values_scalar.size(); k++ )
                {
                    OpCounter::impl::OpCounterVector<T, size> test1 = fma_op(
                        test_values[i], test_values[j],
                        OpCounter::OpCounter<T>(test_values_scalar[k]));
                    
                    OpCounter::impl::OpCounterVector<T, size> test2;
                    for( std::size_t l = 0; l < size; l++ )
                    {
                        test2._v[l] = op(test_values[i]._v[l], 
                                      test_values[j]._v[l], 
                                      OpCounter::OpCounter<T>(test_values_scalar[k]));
                    }
                        
                    REQUIRE(almost_equals_vec<T, size>(
                        OpCounter::impl::to_original(test1), 
                        OpCounter::impl::to_original(test2)));
                }
            }
        }
        
        //vector - scalar - vector
        for( std::size_t i = 0; i < test_values.size(); i++ )
        {
            for( std::size_t j = 0; j < test_values_scalar.size(); j++ )
            {
                for( std::size_t k = 0; k < test_values.size(); k++ )
                {
                    OpCounter::impl::OpCounterVector<T, size> test1 = fma_op(
                        test_values[i], OpCounter::OpCounter<T>(
                        test_values_scalar[j]), test_values[k]);
                    
                    OpCounter::impl::OpCounterVector<T, size> test2;
                    for( std::size_t l = 0; l < size; l++ )
                    {
                        test2._v[l] = op(test_values[i]._v[l], 
                            OpCounter::OpCounter<T>(test_values_scalar[j]), 
                            test_values[k]._v[l]);
                    }
                        
                    REQUIRE(almost_equals_vec<T, size>(
                        OpCounter::impl::to_original(test1), 
                        OpCounter::impl::to_original(test2)));
                }
            }
        }
        
        //scalar - vector - vector
        for( std::size_t i = 0; i < test_values_scalar.size(); i++ )
        {
            for( std::size_t j = 0; j < test_values.size(); j++ )
            {
                for( std::size_t k = 0; k < test_values.size(); k++ )
                {
                    OpCounter::impl::OpCounterVector<T, size> test1 = fma_op(
                        OpCounter::OpCounter<T>(test_values_scalar[i]), 
                        test_values[j], test_values[k]);
                    
                    OpCounter::impl::OpCounterVector<T, size> test2;
                    for( std::size_t l = 0; l < size; l++ )
                    {
                        test2._v[l] = op(OpCounter::OpCounter<T>(
                                    test_values_scalar[i]), 
                                    test_values[j]._v[l], 
                                    test_values[k]._v[l]);
                    }
                        
                    REQUIRE(almost_equals_vec<T, size>(
                        OpCounter::impl::to_original(test1), 
                        OpCounter::impl::to_original(test2)));
                }
            }
        }
        
        //vector - scalar - scalar
        for( std::size_t i = 0; i < test_values.size(); i++ )
        {
            for( std::size_t j = 0; j < test_values_scalar.size(); j++ )
            {
                for( std::size_t k = 0; k < test_values_scalar.size(); k++ )
                {
                    OpCounter::impl::OpCounterVector<T, size> test1 = fma_op(
                        test_values[i], 
                        OpCounter::OpCounter<T>(test_values_scalar[j]), 
                        OpCounter::OpCounter<T>(test_values_scalar[k]));
                    
                    OpCounter::impl::OpCounterVector<T, size> test2;
                    for( std::size_t l = 0; l < size; l++ )
                    {
                        test2._v[l] = op(test_values[i]._v[l], 
                                      OpCounter::OpCounter<T>(test_values_scalar[j]), 
                                      OpCounter::OpCounter<T>(test_values_scalar[k]));
                    }
                        
                    REQUIRE(almost_equals_vec<T, size>(
                        OpCounter::impl::to_original(test1), 
                        OpCounter::impl::to_original(test2)));
                }
            }
        }
        
        //scalar - vector - scalar
        for( std::size_t i = 0; i < test_values.size(); i++ )
        {
            for( std::size_t j = 0; j < test_values.size(); j++ )
            {
                for( std::size_t k = 0; k < test_values.size(); k++ )
                {
                    OpCounter::impl::OpCounterVector<T, size> test1 = fma_op(
                        OpCounter::OpCounter<T>(test_values_scalar[i]), 
                        test_values[j], 
                        OpCounter::OpCounter<T>(test_values_scalar[k]));
                    
                    OpCounter::impl::OpCounterVector<T, size> test2;
                    for( std::size_t l = 0; l < size; l++ )
                    {
                        test2._v[l] = op(OpCounter::OpCounter<T>(test_values_scalar[i]), 
                                      test_values[j]._v[l], 
                                      OpCounter::OpCounter<T>(test_values_scalar[k]));
                    }
                        
                    REQUIRE(almost_equals_vec<T, size>(
                        OpCounter::impl::to_original(test1), 
                        OpCounter::impl::to_original(test2)));
                }
            }
        }
        
        //scalar - scalar - vector
        for( std::size_t i = 0; i < test_values_scalar.size(); i++ )
        {
            for( std::size_t j = 0; j < test_values_scalar.size(); j++ )
            {
                for( std::size_t k = 0; k < test_values.size(); k++ )
                {
                    OpCounter::impl::OpCounterVector<T, size> test1 = fma_op(
                        OpCounter::OpCounter<T>(test_values_scalar[i]), 
                        OpCounter::OpCounter<T>(test_values_scalar[j]), 
                        test_values[k]);
                    
                    OpCounter::impl::OpCounterVector<T, size> test2;
                    for( std::size_t l = 0; l < size; l++ )
                    {
                        test2._v[l] = op(OpCounter::OpCounter<T>(test_values_scalar[i]), 
                                      OpCounter::OpCounter<T>(test_values_scalar[j]), 
                                      test_values[k]._v[l]);
                    }
                        
                    REQUIRE(almost_equals_vec<T, size>(
                        OpCounter::impl::to_original(test1), 
                        OpCounter::impl::to_original(test2)));
                }
            }
        }
    }
}

/** Since horizontal add is special, it needs its own testing function
 * \param data_name datatype name as string, used for test section naming
 * \param op_name operator name as string, used for test section naming
 * \param test_values a std::vector of values of the datatype to be tested
 */
template<typename T, int size>
void vector_test_impl_h_add(std::string data_name, std::string op_name, 
            std::vector<OpCounter::impl::OpCounterVector<T, size>>& test_values)
{
    SECTION("Testing " + op_name + " for OpCounterVector<" + data_name + ">")
    {
        for( std::size_t i = 0; i < test_values.size(); i++ )
        {
            T test1 = 0;
            for( std::size_t j = 0; j < size; j++ )
            {
                test1 += OpCounter::impl::to_original(test_values[i])[j];
            }
            
            OpCounter::OpCounter<T> test2 = OpCounter::impl::horizontal_add
                (test_values[i]);
            
            REQUIRE(test1 == test2);
        }
    }
}

/** Since select is also special, it needs its another testing function
 * \param data_name datatype name as string, used for test section naming
 * \param op_name operator name as string, used for test section naming
 * \param test_values a std::vector of values of the datatype to be tested
 */
template<typename T, int size>
void vector_test_impl_select(std::string data_name, std::string op_name, 
            std::vector<OpCounter::impl::OpCounterVector<T, size>>& test_values)
{
    SECTION("Testing " + op_name + " for OpCounterVector<" + data_name + ">")
    {
        std::vector<OpCounter::impl::BooleanVector<T, size>> test_values_bool(5);
            
        for( std::size_t i = 0; i < test_values_bool.size(); i++ )
        {
            test_values_bool[1][i] = false;
        }
            
        for( std::size_t i = 0; i < test_values_bool.size(); i++ )
        {
            test_values_bool[2][i] = true;
        }
            
        for( std::size_t i = 0; i < test_values_bool.size(); i++ )
        {
            test_values_bool[3][i] = (i < (size/2));
        }
            
        for( std::size_t i = 0; i < test_values_bool.size(); i++ )
        {
            test_values_bool[4][i] = (i >= (size/2));
        }
            
        for( std::size_t i = 0; i < test_values_bool.size(); i++ )
        {
            test_values_bool[5][i] = (i%2 == 0);
        }
        
        for( std::size_t i = 0; i < test_values_bool.size(); i++ )
        {
            for( std::size_t j = 0; j < test_values.size(); j++ )
            {
                for( std::size_t k = 0; k < test_values.size(); k++ )
                {
                    OpCounter::impl::OriginalVector<T, size> test1 =
                        select(test_values_bool[i], test_values[j], test_values[k]);
                        
                    
                    OpCounter::impl::OriginalVector<T, size> test2 =
                        select(test_values_bool[i], 
                               OpCounter::impl::to_original(test_values[j]), 
                               OpCounter::impl::to_original(test_values[k]));
                        
                    REQUIRE(test1 == test2);
                }
            }
        }
    }
}
