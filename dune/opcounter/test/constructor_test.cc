#include <Catch2/single_include/catch.hpp>
#include <dune/opcounter/opcounter.hh>

TEST_CASE("Testing constructor correctness for OpCounter<double>")
{
    double test_object = 1.0;
    double test_object2 = 0.0;
    
    // Construct from an instance of the underlying type
    OpCounter::OpCounter<double> instance(test_object);
    
    // Construct from an rvalue reference of the underlying type
    OpCounter::OpCounter<double> rvalue(test_object + test_object2);
    
    //TODO: Construct from a char array (often used to define multiprecision literals)
    
    // Use default constructor and assign a value of the underlying type
    OpCounter::OpCounter<double> default_instance = test_object;
    
    // Use default constructor and assign a reference to the underlying type
    OpCounter::OpCounter<double> default_rvalue = test_object + test_object2;
    
    //TODO: iostream tests?
    // strstream

    REQUIRE(instance == rvalue);
    REQUIRE(instance == default_instance);
    REQUIRE(instance == default_rvalue);
}
